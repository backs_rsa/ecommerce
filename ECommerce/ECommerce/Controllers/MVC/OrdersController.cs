﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ECommerce.Classes;
using ECommerce.Models;

namespace ECommerce.Controllers
{
    [Authorize(Roles = "User")]
    public class OrdersController : Controller
    {
        private readonly ECommerceContext _db = new ECommerceContext();
        private User _user;

        // GET: Orders
        public ActionResult Index()
        {
            _user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            var orders = _db.Orders.Include(o => o.Customer).Include(o => o.State);
            return View(orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var order = _db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            _user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            if (_user == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(_user.CompanyId), "CustomerId", "FullName");
            var tmp = new NewOrderView()
            {
                Date = DateTime.Now,
                Details = _db.OrderDetailTmps.Where(odt => odt.UserName == User.Identity.Name).ToList()
            };
            return View(tmp);
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewOrderView view)
        {
            if (ModelState.IsValid)
            {
                var response = MovementsHelper.NewOrder(view, User.Identity.Name);
                if (response.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                ModelState.AddModelError(string.Empty, response.Message);
            }

            _user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(_user.CompanyId), "CustomerId", "FullName");
            view.Details = _db.OrderDetailTmps.Where(odt => odt.UserName == User.Identity.Name).ToList();
            return View(view);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = _db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(_db.Customers, "CustomerId", "UserName", order.CustomerId);
            ViewBag.StateId = new SelectList(_db.States, "StateId", "Description", order.StateId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderId,CustomerId,StateId,Date,Remarks")] Order order)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(order).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(_db.Customers, "CustomerId", "UserName", order.CustomerId);
            ViewBag.StateId = new SelectList(_db.States, "StateId", "Description", order.StateId);
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = _db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = _db.Orders.Find(id);
            _db.Orders.Remove(order);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult AddProduct()
        {
            _user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            if (_user == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(_user.CompanyId, false), "ProductId", "Description");
            return PartialView();
        }

        [HttpPost]
        public ActionResult AddProduct(AddProductView view)
        {
            _user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            if (_user == null)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                var orderDetailTmp = _db.OrderDetailTmps.FirstOrDefault(odt => odt.UserName == User.Identity.Name
                 && odt.ProductId == view.ProductId);
                if (orderDetailTmp == null)
                {
                    var product = _db.Products.Find(view.ProductId);
                    orderDetailTmp = new OrderDetailTmp()
                    {
                        Description = product.Description,
                        Price = product.Price,
                        ProductId = product.ProductId,
                        Quantity = view.Quantity,
                        TaxRate = product.Tax.Rate,
                        UserName = User.Identity.Name
                    };
                    _db.OrderDetailTmps.Add(orderDetailTmp);
                }
                else
                {
                    orderDetailTmp.Quantity += view.Quantity;
                    _db.Entry(orderDetailTmp).State = EntityState.Modified;
                }
                _db.SaveChanges();
                return RedirectToAction("Create");
            }
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(_user.CompanyId), "ProductId", "Description");
            return PartialView(view);
        }

        public ActionResult DeleteProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var orderDetailTmps = _db.OrderDetailTmps.FirstOrDefault(odt => odt.ProductId == id
            && odt.UserName == User.Identity.Name);

            if (orderDetailTmps == null)
            {
                return HttpNotFound();
            }
            _db.OrderDetailTmps.Remove(orderDetailTmps);
            _db.SaveChanges();

            return RedirectToAction("Create");
        }
    }
}
