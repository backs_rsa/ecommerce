﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECommerce.Models;

namespace ECommerce.Controllers
{
    public class GenericController : Controller
    {
        private readonly ECommerceContext _db = new ECommerceContext();

        public JsonResult GetCities(int departmentId)
        {
            _db.Configuration.ProxyCreationEnabled = false;
            var cities = _db.Cities.Where(c => c.DepartmentId == departmentId);
            return Json(cities);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}