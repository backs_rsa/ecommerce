﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECommerce.Classes;
using ECommerce.Models;

namespace ECommerce.Controllers
{
    [Authorize(Roles = "User")]
    public class CustomersController : Controller
    {
        private readonly ECommerceContext _db = new ECommerceContext();
        private User _user;

        // GET: Customers
        public ActionResult Index()
        {
            _user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            var qry = (from cu in _db.Customers
                       join cc in _db.CompanyCustomers on cu.CustomerId equals cc.CustomerId
                       join co in _db.Companies on cc.CompanyId equals co.CompanyId
                       where co.CompanyId == _user.CompanyId
                       select new { cu }).ToList();

            var customers = qry.Select(item => item.cu).ToList();

            return View(customers.ToList());
        }

        // GET: Customers/Details/5
        public ActionResult Details(int? id)
        {
            //TODO checar las demas acciones de este controlador ya que no funcionan por la nueva relacion
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = _db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }

            return View(customer);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(0), "CityId", "Name");
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name");
            //_user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            //if (_user == null)
            //{
            //    return RedirectToAction("Index", "Home");
            //}
            //var customer = new Customer()
            //{
            //    CompanyId = _user.CompanyId
            //};
            //return View(customer);
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                using (var tran = _db.Database.BeginTransaction(IsolationLevel.Serializable))
                {
                    try
                    {
                        _db.Customers.Add(customer);
                        var response = DbHelper.SaveChanges(_db);
                        if (response.Succeeded)
                        {
                            UsersHelper.CreateUserAsp(customer.UserName, "Customer");

                            _user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
                            if (_user == null)
                            {
                                return RedirectToAction("Index", "Home");
                            }
                            var companyCustomer = new CompanyCustomer()
                            {
                                CompanyId = _user.CompanyId,
                                CustomerId = customer.CustomerId
                            };
                            _db.CompanyCustomers.Add(companyCustomer);
                            _db.SaveChanges();
                            tran.Commit();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, response.Message);
                            tran.Rollback();
                        }
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                        tran.Rollback();
                    }
                }
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(customer.DepartmentId), "CityId", "Name", customer.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", customer.DepartmentId);
            return View(customer);
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = _db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(customer.DepartmentId), "CityId", "Name", customer.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", customer.DepartmentId);
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CustomerId,CompanyId,UserName,FirstName,LastName,Phone,Address,DepartmentId,CityId")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Entry(customer).State = EntityState.Modified;
                    var response = DbHelper.SaveChanges(_db);
                    if (response.Succeeded)
                    {
                        using (var db2 = new ECommerceContext())
                        {
                            var currentUser = db2.Customers.Find(customer.CustomerId);

                            if (currentUser.UserName != customer.UserName)
                            {
                                UsersHelper.UpdateUserName(currentUser.UserName, customer.UserName);
                            }
                        }
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, response.Message);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(customer.DepartmentId), "CityId", "Name", customer.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", customer.DepartmentId);
            return View(customer);
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = _db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var customer = _db.Customers.Find(id);
            _user = _db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            if (_user == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var companyCustomer =
                _db.CompanyCustomers.SingleOrDefault(
                    c => c.CompanyId == _user.CompanyId && c.CustomerId == customer.CustomerId);

            using (var tran = _db.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    _db.CompanyCustomers.Remove(companyCustomer);
                    _db.Customers.Remove(customer);

                    var response = DbHelper.SaveChanges(_db);
                    if (response.Succeeded)
                    {
                        tran.Commit();
                        return RedirectToAction("Index");
                    }
                    tran.Rollback();
                    ModelState.AddModelError(string.Empty, response.Message);
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            customer.City = _db.Cities.FirstOrDefault(c => c.CityId == customer.CityId);
            //customer.Company = _db.Companies.FirstOrDefault(c => c.CompanyId == customer.CompanyId);
            customer.Department = _db.Departments.FirstOrDefault(c => c.DepartmentId == customer.DepartmentId);
           
            return View(customer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
