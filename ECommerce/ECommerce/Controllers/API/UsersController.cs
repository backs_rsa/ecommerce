﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ECommerce.Classes;
using ECommerce.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json.Linq;

namespace ECommerce.Controllers.API
{
    [RoutePrefix("api/Users")]//vamos a acceder a este controlador por este prefijo
    public class UsersController : ApiController
    {
        private readonly ECommerceContext _db = new ECommerceContext();

        [HttpPost]
        [Route("Login")]// route: api/Users/login
        public IHttpActionResult Login(JObject form)
        {
            string email;
            string password;
            dynamic jsonObject = form;
            try
            {
                email = jsonObject.Email.Value;
                password = jsonObject.Password.Value;
            }
            catch
            {
                return BadRequest("Incorrect Call.");
            }

            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userAsp = userManager.Find(email, password);

            if (userAsp == null)
                return BadRequest("Wrong user or Password");

            _db.Configuration.ProxyCreationEnabled = false;
            var user = _db.Users
                .Include(u => u.City)
                .Include(u => u.Department)
                .Include(u => u.Company)
                .FirstOrDefault(u => u.UserName == email);

            if (user == null)
                return BadRequest("Wrong user or Password");

            var userResponse = new UserResponse()
            {
                Address = user.Address,
                CityId = user.CityId,
                CityName = user.City.Name,
                Company = user.Company,
                DepartmentId = user.DepartmentId,
                DepartmentName = user.Department.Name,
                FirstName = user.FirstName,
                IsAdmin = userManager.IsInRole(userAsp.Id, "Admin"),
                IsCustomer = userManager.IsInRole(userAsp.Id, "Customer"),
                IsSupplier = userManager.IsInRole(userAsp.Id, "Supplier"),
                IsUser = userManager.IsInRole(userAsp.Id, "User"),
                LastName = user.LastName,
                Phone = user.Phone,
                Photo = user.Photo,
                UserId = user.UserId,
                UserName = user.UserName,
                //TODO video 61 hasta aqui
            };

            return Ok(userResponse);
        }


        // GET: api/Users
        public IQueryable<User> GetUsers()
        {
            _db.Configuration.ProxyCreationEnabled = false;
            return _db.Users;
        }

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            _db.Configuration.ProxyCreationEnabled = false;
            User user = _db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserId)
            {
                return BadRequest();
            }

            _db.Entry(user).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.Users.Add(user);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = user.UserId }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = _db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            _db.Users.Remove(user);
            _db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return _db.Users.Count(e => e.UserId == id) > 0;
        }
    }
}