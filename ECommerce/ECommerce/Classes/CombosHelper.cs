﻿using ECommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ECommerce.Classes
{
    public class CombosHelper : IDisposable
    {
        private static readonly ECommerceContext Db = new ECommerceContext();

        public static List<Department> GetDepartments()
        {
            var departments = Db.Departments.ToList();
            departments.Add(new Department()
            {
                DepartmentId = 0,
                Name = "[Select a department...]"
            });
            departments = departments.OrderBy(d => d.Name).ToList();
            return departments;
        }

        public static List<City> GetCities(int departmentId)
        {
            var cities = Db.Cities.Where(c => c.DepartmentId == departmentId).ToList();
            cities.Add(new City()
            {
                CityId = 0,
                Name = "[Select a City...]"
            });
            return cities.OrderBy(d => d.Name).ToList();
        }

        public static List<Company> GetCompanies()
        {
            var companies = Db.Companies.ToList();
            companies.Add(new Company()
            {
                CompanyId = 0,
                Name = "[Select a company...]"
            });
            return companies.OrderBy(d => d.Name).ToList();
        }


        public void Dispose()
        {
            Db.Dispose();
        }

        public static List<Category> GetCategories(int companyId)
        {
            var categories = Db.Categories
                .Where(c => c.CompanyId == companyId).ToList();
            categories.Add(new Category()
            {
                CategoryId = 0,
                Description = "[Select a category...]"
            });
            return categories.OrderBy(d => d.Description).ToList();
        }

        public static List<Tax> GetTaxes(int companyId)
        {
            var taxes = Db.Taxes
                .Where(c => c.CompanyId == companyId).ToList();
            taxes.Add(new Tax()
            {
                TaxId = 0,
                Description = "[Select a tax...]"
            });
            return taxes.OrderBy(d => d.Description).ToList();
        }

        public static List<Customer> GetCustomers(int companyId)
        {
            var qry = (from cu in Db.Customers
                       join cc in Db.CompanyCustomers on cu.CustomerId equals cc.CustomerId
                       join co in Db.Companies on cc.CompanyId equals co.CompanyId
                       where co.CompanyId == companyId
                       select new { cu }).ToList();

            var customers = qry.Select(item => item.cu).ToList();

            customers.Add(new Customer()
            {
                CustomerId = 0,
                FirstName = "[Select a Customer...]"
            });

            return customers.OrderBy(c => c.FirstName).ThenBy(c => c.LastName).ToList();
        }

        public static List<Product> GetProducts(int companyId)
        {
            var products = Db.Products
              .Where(p => p.CompanyId == companyId).ToList();
            products.Add(new Product()
            {
                ProductId = 0,
                Description = "[Select a Product...]"
            });
            return products.OrderBy(p => p.Description).ToList();
        }

        public static List<Product> GetProducts(int companyId, bool sw)
        {
            var products = Db.Products.Where(p => p.CompanyId == companyId).ToList();
            return products.OrderBy(p => p.Description).ToList();
        }

    }
}