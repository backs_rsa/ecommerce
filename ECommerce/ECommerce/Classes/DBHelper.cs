﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECommerce.Models;

namespace ECommerce.Classes
{
    public class DbHelper
    {
        public static Response SaveChanges(ECommerceContext db)
        {
            try
            {
                db.SaveChanges();
                return new Response() { Succeeded = true };
            }
            catch (Exception exception)
            {
                var response = new Response() { Succeeded = false };
                if (exception.InnerException?.InnerException != null && exception.InnerException.InnerException.Message.Contains("_Index"))
                {
                    response.Message = "There are a record with the same value";
                }
                else if (exception.InnerException?.InnerException != null &&
                         exception.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    response.Message = "The record can't be delete because it has related records";

                }
                else
                {
                    response.Message = exception.Message;
                }
                return response;
            }
        }


        public static int GetState(string description, ECommerceContext db)
        {
            var state = db.States.FirstOrDefault(s => s.Description == description);
            if (state != null) return state.StateId;
            state = new State()
            {
                Description = description
            };
            db.States.Add(state);
            db.SaveChanges();
            return state.StateId;
        }
    }
}