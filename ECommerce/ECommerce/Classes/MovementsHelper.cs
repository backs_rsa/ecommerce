﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ECommerce.Models;

namespace ECommerce.Classes
{
    public class MovementsHelper : IDisposable
    {
        private static readonly ECommerceContext Db = new ECommerceContext();





        public static Response NewOrder(NewOrderView view, string userName)
        {
            using (var tran = Db.Database.BeginTransaction(IsolationLevel.Serializable))
            {
                try
                {
                    var user = Db.Users.FirstOrDefault(u => u.UserName == userName);

                    var order = new Order()
                    {
                        CompanyId = user.CompanyId,
                        CustomerId = view.CustomerId,
                        Date = view.Date,
                        Remarks = view.Remarks,
                        StateId = DbHelper.GetState("Created", Db)
                    };

                    Db.Orders.Add(order);
                    Db.SaveChanges();
                    var details = Db.OrderDetailTmps.Where(odt => odt.UserName == userName).ToList();

                    foreach (var orderDetailTmp in details)
                    {
                        var orderDetail = new OrderDetail()
                        {
                            Description = orderDetailTmp.Description,
                            OrderId = order.OrderId,
                            Price = orderDetailTmp.Price,
                            ProductId = orderDetailTmp.ProductId,
                            Quantity = orderDetailTmp.Quantity,
                            TaxRate = orderDetailTmp.TaxRate
                        };
                        Db.OrderDetails.Add(orderDetail);
                        Db.OrderDetailTmps.Remove(orderDetailTmp);
                    }
                    Db.SaveChanges();
                    tran.Commit();
                    return new Response()
                    {
                        Succeeded = true
                    };
                }
                catch (Exception exception)
                {
                    tran.Rollback();
                    return new Response()
                    {
                        Message = exception.Message,
                        Succeeded = false
                    };
                }
            }
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}